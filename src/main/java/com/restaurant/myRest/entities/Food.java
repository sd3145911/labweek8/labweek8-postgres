package com.restaurant.myRest.entities;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Table(name = "tb_foods")
@Entity()
@NoArgsConstructor
public class Food {
    @Column(name = "_id") @Id @GeneratedValue(strategy = GenerationType.UUID)
    private String id;
    @Column(name = "nm_foods")
    private String foodName;
    @Column(name = "vl_price")
    private double price;
    @Column(name = "qt_amount")
    private int amount;

}
