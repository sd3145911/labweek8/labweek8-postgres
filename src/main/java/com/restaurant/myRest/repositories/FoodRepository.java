package com.restaurant.myRest.repositories;

import com.restaurant.myRest.entities.Food;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FoodRepository extends JpaRepository<Food, String> {

}
