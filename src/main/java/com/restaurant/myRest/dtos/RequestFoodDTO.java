package com.restaurant.myRest.dtos;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

public record RequestFoodDTO(
        @NotEmpty @NotNull
        String foodName,
        @Min(0)
        double price,
        @Min(1)
        int amount) {
}
